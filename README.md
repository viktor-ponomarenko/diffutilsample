# Sample of using DiffUtil
## What is DiffUtil ##
As of 24.2.0, RecyclerView support library, v7 package offers really handy utility class called DiffUtil. This class finds the difference between two lists and provides the updated list as an output. This class is used to notify updates to a RecyclerView Adapter.
It uses **Eugene W. Myers’s difference algorithm** to calculate the minimal number of updates.
## How to use? ##
DiffUtil.Callback is an abstract class and used as callback class by DiffUtil while calculating the difference between two lists. It has four abstract methods and one non-abstract method. You have to extend it and override all its methods-
getOldListSize()– Return the size of the old list.
getNewListSize()– Return the size of the new list.
areItemsTheSame(int oldItemPosition, int newItemPosition)– It decides whether two objects are representing same items or not.
areContentsTheSame(int oldItemPosition, int newItemPosition)– It decides whether two items have same data or not. This method is called by DiffUtil only if areItemsTheSame() returns true.
getChangePayload(int oldItemPosition, int newItemPosition)– If areItemTheSame() returns true and areContentsTheSame() returns false than DiffUtil utility calls this method to get a payload about the change.

## Performance ##
DiffUtil requires O(N) space to find the minimal number of addition and removal operations between the two lists. It’s expected performance is O(N + D²) where N is the total number of added and removed items and D is the length of the edit script. You can walk through the official page of Android for more performance figures.
Used links:
* [Using DiffUtil in Android RecyclerView](https://medium.com/@iammert/using-diffutil-in-android-recyclerview-bdca8e4fbb00)
* [RecyclerView++ with DiffUtil](https://geoffreymetais.github.io/code/diffutil/)
* [Get Threading Right with DiffUtil](https://medium.com/@jonfhancock/get-threading-right-with-diffutil-423378e126d2)
* [DiffUtil off the UI thread](https://geoffreymetais.github.io/code/diffutil-threading/)
* [Smart way to update RecyclerView using DiffUtil](https://android.jlelse.eu/smart-way-to-update-recyclerview-using-diffutil-345941a160e0)
* [DiffUtil in details](https://android.jlelse.eu/diffutil-in-details-48b76fd59630)
* [DiffUtil is a must!](https://proandroiddev.com/diffutil-is-a-must-797502bc1149)