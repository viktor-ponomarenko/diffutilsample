package com.davinctor.diffutilsample;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 20.11.2017.
 */
class PersonPayloads extends ArrayList<Integer> {

    public static final int ID_PAYLOAD_TYPE = 0;
    public static final int AGE_PAYLOAD_TYPE = 1;
    public static final int NAME_PAYLOAD_TYPE = 2;

}
