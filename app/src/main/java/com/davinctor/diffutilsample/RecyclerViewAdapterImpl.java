package com.davinctor.diffutilsample;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 15.11.2017.
 */
class RecyclerViewAdapterImpl extends RecyclerView.Adapter<RecyclerViewAdapterImpl.ViewHolder> {

    private final LayoutInflater mLayoutInflater;
    private final List<Person> mPersonList;

    RecyclerViewAdapterImpl(@NonNull Context context,
                            @NotNull List<Person> personList) {
        mLayoutInflater = LayoutInflater.from(context);
        mPersonList = personList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater.inflate(R.layout.text_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            Person item = mPersonList.get(position);
            for (Object payload : payloads) {
                if (payload instanceof PersonPayloads) {
                    for (int payloadType : ((PersonPayloads) payload)) {
                        updatePersonInfo(holder, item, payloadType);
                    }
                }
            }
        }
    }

    private void updatePersonInfo(ViewHolder holder, Person item, int payloadType) {
        switch (payloadType) {
            case PersonPayloads.ID_PAYLOAD_TYPE:
                holder.bindId(item.getId());
                break;
            case PersonPayloads.AGE_PAYLOAD_TYPE:
                holder.bindAge(item.getAge());
                break;
            case PersonPayloads.NAME_PAYLOAD_TYPE:
                holder.bindName(item.getName());
                break;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mPersonList.get(position));
    }

    @Override
    public int getItemCount() {
        return mPersonList.size();
    }

    final static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTextViewId;
        private final TextView mTextViewAge;
        private final TextView mTextViewName;

        ViewHolder(View itemView) {
            super(itemView);
            mTextViewId = itemView.findViewById(R.id.text_view_id);
            mTextViewAge = itemView.findViewById(R.id.text_view_age);
            mTextViewName = itemView.findViewById(R.id.text_view_name);
        }

        @SuppressLint("SetTextI18n")
        void bind(@NonNull Person person) {
            bindId(person.getId());
            bindAge(person.getAge());
            bindName(person.getName());
        }

        @SuppressLint("SetTextI18n")
        void bindId(int id) {
            mTextViewId.setText("ID: " + id);
        }

        @SuppressLint("SetTextI18n")
        void bindAge(int age) {
            mTextViewAge.setText("AGE: " + age);
        }

        @SuppressLint("SetTextI18n")
        void bindName(String name) {
            mTextViewName.setText("NAME" + name);
        }

    }

}
