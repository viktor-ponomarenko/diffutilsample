package com.davinctor.diffutilsample;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;

import java.util.List;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenkoющ@gmail.com
 * @date 15.11.2017.
 */
class PersonDiffUtilCallbackImpl extends DiffUtil.Callback {

    private final List<Person> mOldPersons;
    private final List<Person> mNewPersons;

    PersonDiffUtilCallbackImpl(@NonNull List<Person> oldPersons, @NonNull List<Person> newPersons) {
        mOldPersons = oldPersons;
        mNewPersons = newPersons;
    }

    @Override
    public int getOldListSize() {
        return mOldPersons.size();
    }

    @Override
    public int getNewListSize() {
        return mNewPersons.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldPersons.get(oldItemPosition).getId() == mNewPersons.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldPersons.get(oldItemPosition).equals(mNewPersons.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        Person oldItem = mOldPersons.get(oldItemPosition);
        Person newItem = mNewPersons.get(newItemPosition);
        PersonPayloads personPayloads = new PersonPayloads();
        if (oldItem.getId() != newItem.getId()) {
            personPayloads.add(PersonPayloads.ID_PAYLOAD_TYPE);
        }
        if (oldItem.getAge() != newItem.getAge()) {
            personPayloads.add(PersonPayloads.AGE_PAYLOAD_TYPE);
        }
        if (!TextUtils.equals(oldItem.getName(), newItem.getName())) {
            personPayloads.add(PersonPayloads.NAME_PAYLOAD_TYPE);
        }
        return personPayloads;
    }
}
