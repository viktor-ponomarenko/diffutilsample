package com.davinctor.diffutilsample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 15.11.2017.
 */
public class MainActivity extends AppCompatActivity implements MainContract.View {

    private final List<Person> mPersonList = new ArrayList<>();
    private final MainContract.Presenter mPresenter = new MainPresenterImpl();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler_view);
        mAdapter = new RecyclerViewAdapterImpl(this, mPersonList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.setView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.load();
    }

    @Override
    protected void onStop() {
        mPresenter.release();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_by_age: {
                mPresenter.sortByAge(mPersonList);
                break;
            }
            case R.id.action_sort_by_id: {
                mPresenter.sortById(mPersonList);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void displayPersons(@NonNull List<Person> personList) {
        int count = mPersonList.size();
        mPersonList.clear();
        mAdapter.notifyItemRangeRemoved(0, count);
        mPersonList.addAll(personList);
        mAdapter.notifyItemRangeInserted(0, personList.size());
    }

    @Override
    public void displayPersons(@NonNull List<Person> personList, @NonNull DiffUtil.DiffResult diffResult) {
        mPersonList.clear();
        mPersonList.addAll(personList);
        diffResult.dispatchUpdatesTo(mAdapter);
        mRecyclerView.scrollToPosition(0);
    }
}
