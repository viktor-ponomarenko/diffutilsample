package com.davinctor.diffutilsample;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v7.util.DiffUtil;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 16.11.2017.
 */
class MainPresenterImpl implements MainContract.Presenter {

    private static final String LOG_TAG = "MainPresenterImpl";

    private final Deque<List<Person>> mPendingUpdates = new ArrayDeque<>();
    /**
     * Some things happen another way when changing <code>Executors.newSingleThreadExecutor()</code>
     * to <code>Executors.newFixedThreadPool(4)</code>.
     * <p>First one is when using fixed thread pool
     * with 4 (for example) thread in the pool method <code>#postUpdate()</code> will very often run
     * update one by one, because it will store in own internal stack and order of run operations are
     * different.</p>
     * <p>But when change to using single thread executor appears probability for case when deque
     * clears inside <code>#processUpdatesStack()</code> method and uses last update from
     * <code>mPendingUpdates</code> deque.</p>
     */
    private final ExecutorService mExecutorService = /*Executors.newSingleThreadExecutor()*/Executors.newFixedThreadPool(4);
    private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper());
    private MainContract.View mView;

    @Override
    public void setView(@NonNull MainContract.View view) {
        mView = view;
    }

    @Override
    public void load() {
        if (isAlive()) {
            mView.displayPersons(DataProvider.INSTANCE.getOldPersonList());
        }
    }

    @Override
    public void sortById(@NonNull final List<Person> personList) {
        Log.d(LOG_TAG, "sortById()");
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                final List<Person> newPersonsList = DataProvider.INSTANCE.getOldPersonList();
                simulateLongWait();
                mMainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        postUpdate(personList, newPersonsList);
                    }
                });
            }
        });
    }

    @Override
    public void sortByAge(@NonNull final List<Person> personList) {
        Log.d(LOG_TAG, "sortByAge()");
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                final List<Person> newPersonsList = DataProvider.INSTANCE.sortByAge(new ArrayList<>(personList));
                simulateLongWait();
                mMainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        postUpdate(personList, newPersonsList);
                    }
                });
            }
        });
    }

    @WorkerThread
    private void simulateLongWait() {
        Log.d(LOG_TAG, "simulateLongWait(): start");
        try {
            TimeUnit.SECONDS.sleep((System.currentTimeMillis() % 3) + 1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(LOG_TAG, "simulateLongWait(): end");
    }

    @MainThread
    private void postUpdate(@NonNull List<Person> oldPersonsList,
                            @NonNull List<Person> newPersonsList) {
        mPendingUpdates.add(newPersonsList);
        if (mPendingUpdates.size() == 1) {
            Log.d(LOG_TAG, "postUpdate(): has one update, run it here");
            internalUpdate(oldPersonsList, newPersonsList); // no pending update, let's go
        } else {
            Log.d(LOG_TAG, "postUpdate(): a lot of updates, leave it for intervalUpdate() recursion");
        }
    }

    @MainThread
    private void processUpdatesStack(@NonNull List<Person> currentPersonList) {
        Log.d(LOG_TAG, "processUpdatesStack()");
        mPendingUpdates.remove(); // remove last processed update
        int count = mPendingUpdates.size();
        if (count > 0) {
            if (count > 1) {
                Log.d(LOG_TAG, "processUpdatesStack(): save last one element and clear deque");
                List<Person> lastUpdate = mPendingUpdates.peekLast();
                mPendingUpdates.clear();
                mPendingUpdates.add(lastUpdate);
            }
            Log.d(LOG_TAG, "processUpdatesStack(): deque not empty, run update again");
            internalUpdate(currentPersonList, mPendingUpdates.peek());
        } else {
            Log.d(LOG_TAG, "processUpdatesStack(): nothing to update");
        }
    }

    private void internalUpdate(@NotNull final List<Person> oldPersonsList,
                                @NonNull final List<Person> newPersonsList) {
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                final DiffUtil.Callback diffUtilCallback = new PersonDiffUtilCallbackImpl(
                        oldPersonsList, newPersonsList);
                final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffUtilCallback);
                Log.d(LOG_TAG, "internalUpdate(): calculated");
                mMainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isAlive()) {
                            mView.displayPersons(newPersonsList, diffResult);
                            Log.d(LOG_TAG, "internalUpdate(): posted to ui");
                            processUpdatesStack(newPersonsList);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void release() {
        mExecutorService.shutdownNow();
        mMainThreadHandler.removeCallbacksAndMessages(null);
        mView = null;
    }

    private boolean isAlive() {
        return mView != null;
    }

}
