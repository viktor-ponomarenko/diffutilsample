package com.davinctor.diffutilsample;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import java.util.List;

/**
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 16.11.2017.
 */
interface MainContract {

    interface View {

        void displayPersons(@NonNull List<Person> personList);

        void displayPersons(@NonNull List<Person> personList, @NonNull DiffUtil.DiffResult diffResult);

    }

    interface Presenter {

        void setView(View view);

        void load();

        void sortById(@NonNull List<Person> personList);

        void sortByAge(@NonNull List<Person> personList);

        void release();

    }

}
