package com.davinctor.diffutilsample

/**
 *
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 15.11.2017.
 */
data class Person(val id: Int, val age: Int, val name: String)