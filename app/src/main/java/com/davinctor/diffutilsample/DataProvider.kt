package com.davinctor.diffutilsample

import java.util.*

/**
 *
 * @author Victor Ponomarenko
 * @email victor.ponomarenko@petcube.com
 * @date 15.11.2017.
 */
internal object DataProvider {

    val oldPersonList: List<Person>
        get() {
            val persons = ArrayList<Person>()
            persons.add(Person(1, 20, "John"))
            persons.add(Person(2, 12, "Jack"))
            persons.add(Person(3, 8, "Michael"))
            persons.add(Person(4, 19, "Rafael"))
            persons.add(Person(5, 25, "Donatello"))
            persons.add(Person(6, 8, "Michelangelo"))
            persons.add(Person(7, 12, "Ivan"))
            return persons
        }

    fun sortByAge(oldList: List<Person>): List<Person> {
        Collections.sort(oldList, object : Comparator<Person> {
            override fun compare(person: Person, person2: Person): Int {
                return person.age - person2.age
            }
        })
        return oldList
    }
}